class RenameUserIdToCreatedByCards < ActiveRecord::Migration[5.2]
  def change
    rename_column :cards, :user_id, :created_by
  end
end
