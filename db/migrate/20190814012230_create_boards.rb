class CreateBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :boards do |t|
      t.integer :project_id
      t.string :name
      t.string :status, :limit => 1
      t.timestamps
    end
  end
end
