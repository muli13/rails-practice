class CreatePriorities < ActiveRecord::Migration[5.2]
  def change
    create_table :priorities do |t|
      t.string :name, :limit => 100
      t.text :description
    end
  end
end
