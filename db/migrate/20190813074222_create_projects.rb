class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.integer :user_id
      t.string :name
      t.string :key, :limit => 10
      t.text :url
      t.text :description
      t.string :status, :limit => 1
      t.timestamps
    end
  end
end
