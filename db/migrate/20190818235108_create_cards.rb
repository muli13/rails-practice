class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.integer :sprint_id
      t.string :title, :limit => 100
      t.text :description
      t.integer :board_id
      t.integer :list_id
      t.integer :type_issue_id
      t.integer :priority_id
      t.integer :user_id
      t.datetime :duedate
      t.integer :status, :limit => 1
      t.timestamps
    end
  end
end
