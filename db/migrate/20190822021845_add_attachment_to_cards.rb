class AddAttachmentToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :attachment, :string
  end
end
