class AddDefaultToProject < ActiveRecord::Migration[5.2]
  def up
    change_column :projects, :status, :string, :limit => 1, :default => 1
  end

  def down
    change_column :projects, :status, :string, :limit => 1
  end
end
