class CreateSprints < ActiveRecord::Migration[5.2]
  def change
    create_table :sprints do |t|
      t.integer :board_id
      t.string :name
      # duration in one days
      t.integer :duration, :comment => 'duration in one days'
      t.datetime :start
      t.datetime :end
      t.text :sprint_goal
      t.string :status, :limit => 1
      t.timestamps
    end
  end
end
