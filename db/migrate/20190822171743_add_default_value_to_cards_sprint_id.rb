class AddDefaultValueToCardsSprintId < ActiveRecord::Migration[5.2]
  def change
    change_column :cards, :sprint_id, :integer, :limit => 1, :default => 0
  end
end
