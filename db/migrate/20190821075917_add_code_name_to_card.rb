class AddCodeNameToCard < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :code_name, :string, :limit => 100
  end
end
