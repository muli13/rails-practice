class AddDefaultValueToSprints < ActiveRecord::Migration[5.2]
  def change
    change_column :sprints, :status, :integer, :limit => 1, :default => 0
  end
end
