class AddSortOrderToSprints < ActiveRecord::Migration[5.2]
  def change
    add_column :sprints, :sort_order, :integer, :limit => 1
  end
end
