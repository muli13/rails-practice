class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.integer :user_id
      t.integer :project_id
      t.integer :role, :comment => "1: owner & 2: member"
      t.integer :status, :limit => 1
      t.timestamps
    end
  end
end
