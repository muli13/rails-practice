class CreateTypeIssueCustoms < ActiveRecord::Migration[5.2]
  def change
    create_table :type_issue_customs do |t|
      t.integer :board_id
      t.string :name
      t.text :description
      t.timestamps
    end
  end
end
