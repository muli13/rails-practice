class CreateLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.integer :board_id
      t.integer :sprint_id
      t.string :title
      t.integer :sort_order
    end
  end
end
