class AddAssignToToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :assign_to, :integer
  end
end
