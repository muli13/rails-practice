# costum controller devise for registrations
class RegistrationsController < Devise::RegistrationsController
  protected

  # change the path when sign_up
  def after_sign_up_path_for(resource)
    projects_path
  end
end
