class ApplicationController < ActionController::Base
    include Pundit

    protect_from_forgery with: :exception

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    # add a new field can access in devise
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:fullname])
    end

    # change default path devise when sign in2
    def after_sign_in_path_for(resource)
	    stored_location_for(resource) || projects_path
    end
end
