class ListsController < ApplicationController
  layout 'main_layout'

  def index
    get_parameter

    @list = List.list_cards(params[:board_id])
    @list_date = List.list_sprint_date(params[:board_id])
    @sprint = Board.find(params[:board_id]).sprints.find_by(status: 1)

  end

  private

  def get_parameter
    @project_id = params[:project_id]
    @board_id = params[:board_id] ? params[:board_id] : params[:id]
  end
end
