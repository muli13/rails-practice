class PeopleController < ApplicationController
  def create
    @project = Project.find(params[:project_id])
    @person  = @project.people.new(resource_params)

    @person.role = 2
    @person.status = 1

    if @person.save
      redirect_to project_path(@project)
    end
  end

  def destroy
    @person = Person.find(params[:id])

    if @person.destroy
      redirect_to project_path(params[:project_id])
    end
  end

  private

  def resource_params
    params.require(:person).permit(:user_id)
  end
end
