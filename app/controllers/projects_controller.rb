class ProjectsController < ApplicationController
    layout 'main_layout'

    def index
      @project = Project.where(user_id: current_user.id)
      session[:role] = 1

      if @project.empty?
        @person = User.find(current_user.id).people
        selection_project = @person.pluck(:project_id)

        @project = Project.where(id: selection_project)
        session[:role] = 2
      end
    end

    def new
      @project = Project.new
    end

    def create
      project = User.find(current_user.id).projects.new(resource_params)

      if project.save
        redirect_to projects_path
      end
    end

    def show
      @project = Project.find(params[:id])

      # create
      @person = Person.new

      @users = @project.people.pluck(:user_id)
      @userlist = User.where.not(id: @users)
                      .where('id != ?', current_user.id)

      @boards = @project.boards.all
      @member = @project.people.all
    end

    private

    def resource_params
      params.require(:project).permit(:name, :key)
    end
end
