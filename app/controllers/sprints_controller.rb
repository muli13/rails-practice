class SprintsController < ApplicationController
  layout 'main_layout'

  def create
    @sprint = Sprint.new(board_id: params[:board_id])
    params.permit(:board_id)

    if @sprint.save
      redirect_backlog
    end
  end

  def sprint_start
    get_parameter

    @sprint = Sprint.find(params[:id])
    @board = @sprint.board
    @project = @board.project

    # still default
    @duration = Duration.all
  end

  def complete_sprint
    get_parameter

    @sprint = Sprint.to_complete(params[:id])
    if @sprint
      redirect_backlog
    end
  end

  def edit
    get_parameter

    @sprint = Sprint.find(params[:id])
    @board = @sprint.board
    @project = @board.project
  end

  def update
    get_parameter
    @sprint = Sprint.find(params[:id])
    if @sprint.update(resource_params)
      redirect_backlog
    end
  end

  def destroy
    @sprint = Sprint.find(params[:id]);

    if @sprint.destroy
      redirect_backlog
    end
  end

  def redirect_backlog
    @project = Project.find(params[:project_id])
    @board = Board.find(params[:board_id])
    redirect_to backlog_project_board_path(@project, @board)
  end

  private

  def get_parameter
    @project_id = params[:project_id]
    @board_id = params[:board_id] ? params[:board_id] : params[:id]
  end

  def resource_params
    params.require(:sprint).permit(:name, :duration, :start, :end, :sprint_goal)
  end
end
