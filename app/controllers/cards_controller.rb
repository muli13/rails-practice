class CardsController < ApplicationController
  def create
    @card = Card.new(resource_params)
    @card.sprint_id = 0
    @card.board_id = params[:board_id]
    @card.created_by = current_user.id

    if @card.save
      redirect_to backlog_project_board_path(params[:project_id], params[:board_id])
    end

    puts @card.errors.full_messages
  end

  def destroy
    @card = Card.find(params[:id])
    if @card.destroy
      redirect_to backlog_project_board_path(params[:project_id], params[:board_id])
    end
  end

  def update
    @card = Card.find(params[:id])
    @card.created_by = current_user.id

    if @card.update(resource_params)
      redirect_to backlog_project_board_path(params[:project_id], params[:board_id])
    end
  end

  private

  def resource_params
    # add assign_to & user_id change created_by
    params.require(:card).permit(:list_id, :type_issue_id, :title, :description, :attachment, :priority_id, :sprint_id, :created_by, :assign_to, :duedate)
  end
end
