class BoardsController < ApplicationController
  layout 'main_layout'

  # main (board)
  def show
    get_parameter
  end

  # backlog
  def backlog
    get_parameter

    @board = Board.sprint_cards(params[:id])
    @sprint_first = @board.first
    @sprint = @board

    @backlog = Board.find(params[:id]).cards.where(sprint_id: 0)

    # -- still default type_issue --
    @typeissue = TypeIssue.all
    # ------------------------------
  end

  # create issue
  def issue
    get_parameter

    @card = Card.new

    get_issue_default
    # if this finish go to "backlog" again ^ finish
  end

  def edit_issue
    get_parameter
    @card = Card.find(params[:id])

    get_issue_default
    # it's for edit
  end

  private

  def get_parameter
    @project_id = params[:project_id]
    @board_id = params[:board_id] ? params[:board_id] : params[:id]
  end

  def get_issue_default
    # still default ---
    @typeissue = TypeIssue.all
    @priority = Priority.all
    # still default ---

    # @board_id = params[:id]
    @board = Board.find(@board_id)
    @list = @board.lists.all
    @sprint = @board.sprints.where.not(status: 2)

    @project = @board.project

    # fix assign list here !
    @people = @board.project.people
  end
end
