module ApplicationHelper
  # helper fo views table
  def empty_table param, column
    message = "data no available"
    if param.empty?
      return raw('<tr class="empty_table"><td class="text-center" colspan="'+column.to_s+'">'+message+'</td></tr>')
    end
  end

  def show_sprint_start id, detail_id, status
    (id == detail_id) && status == 0
  end

  def role_view param
    param == 1
  end
end
