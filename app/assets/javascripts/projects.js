$(document).on('turbolinks:load', function() {
  $('#name-project').keyup(function() {
    let result;
    let name = $(this).val();
    result = keyProcess(name);

    $('#key-project').val(result);
  });

  function keyProcess(name) {
    let key = "";
    let temp = name.split(/[.\-_]/);

    for (var i = 0; i < temp.length; i++) {
      key += temp[i][0] !== undefined ? temp[i][0] : ""
    }

    return key
  }
});
