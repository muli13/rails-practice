$(document).on('turbolinks:load', function() {
    function compare_dates(date_a, date_b) {
        if (date_a <= date_b) {
            return true
        }
        return false
    }

    $('#duration').click(function() {
        let duration = $(this).val()
        let startdate = moment().format("YYYY-MM-DD");
        let enddate = moment().add(duration, 'days').format("YYYY-MM-DD");

        if (duration) {
            // if value exist ?
            $('#start-sprint').attr('readonly', true)
            $('#end-sprint').attr('readonly', true)

            $('#start-sprint').val(startdate)
            $('#end-sprint').val(enddate)
        } else {
            $('#start-sprint').attr('readonly', false)
            $('#end-sprint').attr('readonly', false)

            $('#end-sprint').val('')
        }
    })

    $(document).on('change', '#start-sprint, #end-sprint', function() {
        let date_a = $('#start-sprint').val()
        let date_b = $('#end-sprint').val()

        if((date_a && date_b) && !compare_dates(date_a, date_b)) {
            $('#end-sprint').val('')
            return alert('the end sprint must more than start sprint !')
        }
    })
});



