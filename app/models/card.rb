class Card < ApplicationRecord
  mount_uploader :attachment, AttachmentUploader

  belongs_to :board
  belongs_to :sprint, optional: true

  belongs_to :list
  belongs_to :priority
  belongs_to :type_issue

  def title_priority
    self.priority.name
  end

  def title_issue
    self.type_issue.name
  end
end
