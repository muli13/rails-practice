class Project < ApplicationRecord
  belongs_to :user

  # okay relation
  has_many :boards

  has_many :people
  has_many :users, :through => :people

  after_create :create_board

  def create_board
    @board = self.boards.new
    @board.name = self.key

    @board.save
  end
end
