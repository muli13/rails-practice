class Person < ApplicationRecord
  belongs_to :user
  belongs_to :project

  def name_with_initial
    self.user.email
  end
end
