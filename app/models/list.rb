class List < ApplicationRecord

  # okay relation
  has_many :cards
  belongs_to :board

  belongs_to :sprint, optional: true

  def self.list_cards param
    lists = Board.find(param).lists
    result = lists.map do |list|
      {
          title: list.title,
          cards: list.cards.where(status: 1)
      }
    end

    return result
  end

  def self.list_sprint_date param
    sprints = Board.find(param).sprints.find_by(status: 1)

    if sprints
      remaining = sprints.duration - (DateTime.now.to_date - sprints.start.to_date).to_i
      result = remaining.to_s + " days remaining"
    else
      result = "0 days remaining"
    end

    return result
  end
end
