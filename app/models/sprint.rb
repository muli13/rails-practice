class Sprint < ApplicationRecord
  has_many :cards
  belongs_to :board

  has_many :lists

  before_create :create_sprint
  before_destroy :update_cards

  # make condition here because it's can be work when update in "duration, start, end sprint" .
  after_update :active_status_cards, :if => :condition_status?

  # it's okay
  def create_sprint
    board = Sprint.where(board_id: self.board_id).last
    str_order = board ? board.sort_order + 1 : 1
    combine = self.board.name + " Sprint " + str_order.to_s

    self.name = combine
    self.sort_order = str_order
  end

  # it's okay
  def update_cards
    Card.where(sprint_id: self.id).update_all(sprint_id: 0)
  end

  # it's okay
  def active_status_cards
    # 1 say if the card is active
    @card = Card.where(sprint_id: self.id).update_all(status: 1)
    @list = List.where(board_id: self.board_id).update_all(sprint_id: self.id)

    self.duration ? self.duration : self.duration = (self.end.to_date - self.start.to_date).to_i
    self.update_columns(:duration => self.duration, :status => 1)
  end

  # this method is for complete the sprint & cards too
  def self.to_complete param
    list = List.where(sprint_id: param).last
    sprint = Sprint.find(param).cards

    # update complete card
    sprint.where(list_id: list.id).update_all(status: 2)

    # update not complete card
    sprint.where.not(list_id: list.id).update_all(:sprint_id => 0, :status => 0)

    # update sprint to complete
    List.where(sprint_id: param).update_all(sprint_id: 0)
    Sprint.find(param).update(status: 2)
  end

  private

  def condition_status?
    self.status == 0 && :duration_changed?
  end
end
