class Board < ApplicationRecord
  # okay relation
  belongs_to :project
  has_many :lists
  has_many :sprints

  has_many :cards

  after_create :create_list

  def create_list
    default_list = ['todo', 'inprogress', 'test', 'done']
    default_list.each_with_index do |item, index|
      list = self.lists.new
      list.title = item
      list.sort_order = index

      list.save()
    end

  end

  def self.sprint_cards param
    sprints = Board.find(param).sprints.where.not(status: 2)
    result = sprints.map do |sprint|
      {
          id: sprint.id,
          name: sprint.name,
          status: sprint.status,
          cards: sprint.cards
      }
    end

    return result
  end
end
