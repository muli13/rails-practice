Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: { registrations: "registrations" }

  # set a devise login to root
  devise_scope :user do
      root to: "devise/sessions#new"
  end

  resources :projects, only: [:index, :new, :create, :show] do
    resources :people, only: [:create, :destroy]
    resources :boards, only: [:index, :show] do
      get :backlog, on: :member
      get :issue, on: :member
      get "issue/:id/edit_issue" => "boards#edit_issue", as: :edit_issue

      resources :lists, only: [:index, :show]
      resources :sprints, only: [:create, :edit, :update, :destroy] do
        put :complete_sprint, on: :member
        get :sprint_start, on: :member
      end
      resources :cards, only: [:create, :update, :destroy]
    end
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
